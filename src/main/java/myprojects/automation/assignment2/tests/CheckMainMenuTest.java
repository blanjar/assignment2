package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class CheckMainMenuTest extends BaseScript{
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getDriver();
        signInToCMS(driver);

        delay(3000);

        List<WebElement> menus = driver.findElements(By.className("maintab"));
        String[] ids = new String[menus.size()];

        int counter = 0;
        for (WebElement menu : menus) {
            ids[counter++] = menu.getAttribute("data-submenu");
        }

        for (int i = 0; i < ids.length; i++) {
            driver.findElement(By.cssSelector("li[data-submenu='"+ids[i]+"']")).click();
            delay(3000);
            String oldPageName = driver.findElement(By.tagName("h2")).getText();
            driver.navigate().refresh();
            delay(3000);
            String newPageName = driver.findElement(By.tagName("h2")).getText();
            if (oldPageName.equals(newPageName)) {
                System.out.
                        println("Page titles match after refresh. Before: " + oldPageName + " After: " + newPageName);
            } else {
                System.out.
                        println("Page titles does not match after refresh. Before: " + oldPageName + " After: " + newPageName);
            }
        }
        driver.quit();
    }
}
