package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginTest extends BaseScript {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getDriver();
        signInToCMS(driver);

        delay(3000);
        driver.findElement(By.className("employee_avatar_small")).click();
        delay(1000);
        driver.findElement(By.id("header_logout")).click();

        driver.quit();
    }
}
