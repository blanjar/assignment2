package myprojects.automation.assignment2;

import myprojects.automation.assignment2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {

    /**
     *
     * @return New instance of {@link WebDriver} object.
     */
    public static WebDriver getDriver() {
        String browser = Properties.getBrowser();
        try {
            WebDriver driver;
            switch (browser) {
                case "internet explorer":
                    System.setProperty(
                            "webdriver.ie.driver",
                            new File(BaseScript.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                    driver = new InternetExplorerDriver();
                    break;
                case "MicrosoftEdge":
                    System.setProperty(
                            "webdriver.ie.driver",
                            new File(BaseScript.class.getResource("/MicrosoftWebDriver.exe").getFile()).getPath());
                    driver = new InternetExplorerDriver();
                    break;
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
                    System.out.println("Step");
                    driver = new FirefoxDriver();
                    break;
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                    driver = new ChromeDriver();
            }
            driver.manage().window().maximize();
            return driver;
        } catch (Throwable e) {
            throw new UnsupportedOperationException("Method doesn't return WebDriver instance");
        }
    }

    /**
     *
     * Delays the thread to the specified milliseconds.
     */
    public static void delay(int period) throws InterruptedException {
        try {
            Thread.sleep(period);
        } catch (Throwable e) {
            throw new InterruptedException("Main thread interrupted.");
        }
    }

    /**
     *
     * Does sign in to the CMS using {@link WebDriver} object.
     */
    public static void signInToCMS(WebDriver driver) throws InterruptedException {
        driver.get(Properties.getBaseAdminUrl());
        delay(5000);
        driver.findElement(By.id("email")).sendKeys(Properties.getCMSLogin());
        driver.findElement(By.id("passwd")).sendKeys(Properties.getCMSPassword());
        driver.findElement(By.name("submitLogin")).click();
    }
}
